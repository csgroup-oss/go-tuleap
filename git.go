//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"net/url"
)

type GitRepository struct {
	Id            int    `json:"id"`
	Label         string `json:"label"`
	Name          string `json:"name"`
	Path          string `json:"path"`
	Description   string `json:"description"`
	DefaultBranch string `json:"default_branch"`
}

type GitFile struct {
	Encoding string `json:"encoding"`
	Size     int    `json:"size"`
	Name     string `json:"name"`
	Path     string `json:"path"`
	Content  string `json:"content"`
}

// The GetGit method returns the Git by id.
//
// See https://tuleap.net/api/explorer/#/git/retrieve%5CTuleap%5CGit%5CREST%5Cv1%5CRepositoryResource
func (c *Client) GetGit(id int) (*GitRepository, error) {
	url := fmt.Sprintf("/api/git/%d", id)

	var t GitRepository
	_, err := c.RequestJSON("GET", url, nil, nil, &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func (c *Client) GetGitFiles(id int, ref string, path string) (*GitFile, error) {
	v := url.Values{}
	v.Set("path_to_file", path)
	v.Set("ref", ref)
	url := fmt.Sprintf("/api/git/%d/files", id)

	var t GitFile
	_, err := c.RequestJSON("GET", url, &v, nil, &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}
