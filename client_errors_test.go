package tuleap

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"testing"
)

const FakeError string = `{"error":{"code":400,"message":"Bad Request: Permission field values must be passed as an array of ugroup ids e.g. \"value\" : {\"granted_groups\" : [158, \"142_3\"]}"}}`

func TestDecodeError(t *testing.T) {
	var errorMsg ClientErrorPayload
	err := json.Unmarshal([]byte(FakeError), &errorMsg)
	if err != nil {
		t.Error(err)
	}
	if errorMsg.Error.Code != 400 {
		t.Errorf("Expecting %d got %d", 400, errorMsg.Error.Code)
	}
}

func TestPayloadDecoding(t *testing.T) {
	var all interface{}
	var kv struct {
		Val string `json:"key"`
	}
	var errMsg ClientErrorPayload
	var tests = []struct {
		body    string
		content interface{}
	}{
		{"", &all},
		{`{"key":"value"}`, &kv},
		{FakeError, &errMsg},
	}
	for i, tt := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {

			// Create a Reader from a raw string
			buffer := bytes.NewBuffer([]byte(tt.body))
			dec := json.NewDecoder(buffer)
			if dec.More() {
				if err := dec.Decode(tt.content); err != nil {
					t.Errorf("Failed to decode payload: %v", err)
				}
				fmt.Printf("%s\n%+v\n", tt.body, tt.content)
			} else {
				fmt.Print("Nothing to read\n")
			}
		})
	}
}
