# go-tuleap

A Tuleap API client library enabling Go programs to interact with [Tuleap](https://www.tuleap.org/).

A companion project called [tuleap-cli](https://gitlab.com/csgroup-oss/tuleap-cli) offers a command line access to the tuleap's API.

## Usage

```golang
import "gitlab.com/csgroup-oss/go-tuleap"
```

Construct a Client:, then use then access to the API.
For example, to find a project by its name:

```golang
auth := tuleap.Client{
    Token: "yourtokengoeshere",
}
tc, err := tuleap.NewClient(server, auth, &tls.Config{InsecureSkipVerify: insecure})
if err != nil {
        log.Fatalf("Failed to create client: %v", err)
}
// Do not iterate on pagination, a single result is expected
projects, _, err := tc.GetProjectsByShortname("my-project", &PaginationOptions{})
```

### Pagination

Most of the endpoints of the [Tuleap's API are paginated](https://docs.tuleap.org/user-guide/integration/rest/collections.html).
In order to request such endpoint, the client as to provide a `tuleap.PaginationOptions` struct and use it to iterate or stop.

A common call is:

```golang
opt := tuleap.PaginationOptions{}
for {
    items, resp, err := tc.FindUsers("john", &opt)
    if err != nil {
        log.Fatalf("Failed to request users: %v", err)
        break
    }

    for _, item := range items {
        // Do something with item
    }

    // Update the page number to get the next page.
    opt.Limit = resp.Limit
    opt.Offset += opt.Limit
    // Exit the loop when we've seen all pages.
    if opt.Offset > resp.Size {
        break
    }
}
```

In order to avoid repeating this pattern, most of the API functions have a `*All` variant returning the result via a channel.

```golang
for item := tc.FindUsersAll("john", errRcv) {
    // Do something with item
}
```

Note that with such construction, due to the async nature of channels, API calls errors cannot be returned directly.
In this case it is necessary to use an ErrorReceiver.
Two implementations of this interface are provided:

* the global `DefaultErrorCallback` variable that prints errors on standard error
* the `CatcherErrorCallback` function that creates an `ErrorReceiver` collecting the error on a given `error` variable.

### Generic call

If the endpoint you are interested on is not already covered, two generics functions can be use to allow such call: `RequestJSON` and `RequestPaginatedJSONAll`.
These selection between these two function is based on the nature of the endpoint: is it paginated or not.
In order to make such a call, in both case you have to provide:

* a `tuleap.Client` 
* the type of method (`GET`, `POST`, `PATCH`, `PUSH`)
* the full path to the endpoint
* a `url.Values` with the query parameters expected by the endpoint

For a simple call, one example could be:

```golang
v := url.Values{"fields":"all"}
path = fmt.Sprintf("/api/artifacts/%d/", 1234)
data := tuleap.RequestJSON[interface{}](tc, "GET", path, &v, nil)
```

For a paginated endpoint, in order to iterate over all values:

```golang
v := url.Values{"fields":"comments"}
path = fmt.Sprintf("/api/artifacts/%d/changesets", 1234)
data := tuleap.RequestPaginatedJSONAll[interface{}](tc, "GET", path, &v, nil, nil)
for datum := range data {
    // Do what you want with each datum
}
```

These functions are generics and thus let you pass a type to retrieve exactly the attributes you need, even if it is not proposed by the library.

For example:

```golang
type MyVersion struct {
    Version string
}
path := "/api/version"
v, err := RequestJSON[MyVersion](tc, "GET", path, nil, nil)
```

## Coverage

See [coverage.md](coverage.md) for the currently covered API endpoints.

## Contributions

See [contribution notes](CONTRIBUTING.md).

## Authors

This project was initially created by Guilhem Bonnefille working at [CS GROUP - France](https://www.csgroup.eu).

## Acknowledgements

This project was inspired by other famous Golang interfaces to other services:

* [go-gitlab](https://github.com/xanzy/go-gitlab)
* [go-github](https://github.com/google/go-github)

## License

Licensed under the Apache License, Version 2.0 (the "License").
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
