//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

type Project struct {
	Id          int      `json:"id"`
	Label       string   `json:"label"`
	ShortName   string   `json:"shortname"`
	Description string   `json:"description"`
	Status      string   `json:"status"`
	Access      string   `json:"access"`
	IsTemplate  bool     `json:"is_template"`
	Resources   []string `json:"resources>type"`
}

type GitJenkinsServer struct {
	Id  int    `json:"id"`
	Url string `json:"url"`
}

type SvnRepository struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
	Url  string `json:"svn_url"`
}
