//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import "fmt"

// GetFrsFiles method return the given file information
//
// See https://tuleap.net/api/explorer/#/frs_files/tuleap%5CFRS%5CREST%5Cv1%5CFileResourceRetrieveId
func (c *Client) GetFrsFile(id int) (*FrsFile, error) {
	u := fmt.Sprintf("/api/frs_files/%d", id)
	var output FrsFile
	_, err := c.RequestJSON("GET", u, nil, nil, &output)
	return &output, err
}

// CreateFrsFile method creates a new file
//
// See https://tuleap.net/api/explorer/#/frs_files/createTuleap%5CFRS%5CREST%5Cv1%5CFileResource
func (c *Client) CreateFrsFile(release_id int, name string, size int64) (*Uploadable, error) {
	u := "/api/frs_files"
	input := map[string]interface{}{
		"release_id": release_id,
		"name":       name,
		"file_size":  size,
	}
	var output Uploadable
	_, err := c.RequestJSON("POST", u, nil, input, &output)
	return &output, err
}
