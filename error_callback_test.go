//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDefault(t *testing.T) {
	// TODO
}

func PassError(err error, err_callback *ErrorCallback) error {
	return err_callback.callback(err, err_callback.data)
}

func TestCatcher(t *testing.T) {
	var err error
	c := CatcherErrorCallback(&err)

	ret := PassError(nil, c)
	assert.Nil(t, ret)
	assert.Equal(t, nil, err)

	my_error := fmt.Errorf("My Error")
	ret = PassError(my_error, c)
	assert.Nil(t, ret)
	assert.Equal(t, my_error, err)
}
