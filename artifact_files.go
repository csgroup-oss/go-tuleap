//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"io"
	"net/url"
)

// The DownloadArtifactFile method gives access the the content of the file.
func (c *Client) DownloadArtifactFile(file FileDescription) (io.ReadCloser, error) {
	path := fmt.Sprintf("/plugins/tracker/attachments/%d-%s",
		file.Id,
		url.PathEscape(file.Name))
	resp, err := c.Request("GET", path, nil, nil, nil)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}
