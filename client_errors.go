package tuleap

import "fmt"

type ClientError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (e ClientError) Error() string {
	return fmt.Sprintf("%d %s", e.Code, e.Message)
}

type ClientErrorPayload struct {
	Error ClientError `json:"error"`
}
