//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"net/url"
)

// The GetTracker method returns the Tracker id.
//
// See https://tuleap.net/api/explorer/#/trackers/tuleap%5CTracker%5CREST%5Cv1%5CTrackersResourceRetrieveId
func (c *Client) GetTracker(id int) (*Tracker, error) {
	url := fmt.Sprintf("/api/trackers/%d", id)

	var t Tracker
	_, err := c.RequestJSON("GET", url, nil, nil, &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

// GetTrackerArtifacts returns the artifacts of the tracker id matching the query, filling the type T.
// The values parameter let defines which fields are included in the response (no fields values by default).
//
// See https://tuleap.net/api/explorer/#/trackers/tuleap%5CTracker%5CREST%5Cv1%5CTrackersResourceRetrieveArtifacts
func GetTrackerArtifacts[T interface{}](c *Client, id int, values string, query string, opt *PaginationOptions) ([]T, *PaginationResponse, error) {
	v := url.Values{}
	v.Set("expert_query", query)
	v.Set("values", values)
	url := fmt.Sprintf("/api/trackers/%d/artifacts", id)
	return RequestPaginatedJSON[T](c, "GET", url, &v, opt, nil)
}

// The GetTrackerArtifacts methods returns the artifacts of the tracker id matching the query and following the values option.
// It is paginated.
func (c *Client) GetTrackerArtifacts(id int, values string, query string, opt *PaginationOptions) ([]Artifact, *PaginationResponse, error) {
	return GetTrackerArtifacts[Artifact](c, id, values, query, opt)
}

// The GetTrackerArtifactsRaw methods returns the artifacts of the tracker id matching the query and following the values option, as raw JSON structure.
// It is paginated.
func (c *Client) GetTrackerArtifactsRaw(id int, values string, query string, opt *PaginationOptions) ([]interface{}, *PaginationResponse, error) {
	return GetTrackerArtifacts[interface{}](c, id, values, query, opt)
}

func GetTrackerArtifactsAll[T interface{}](c *Client, id int, values string, query string, errRcv ErrorReceiver) <-chan T {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]T, *PaginationResponse, error) {
		return GetTrackerArtifacts[T](c, id, values, query, opt)
	}, errRcv)
}

// The GetTrackerArtifactsAll method returns all artifacts of the tracker id matching the query and following the values option.
func (c *Client) GetTrackerArtifactsAll(id int, values string, query string, errRcv ErrorReceiver) <-chan Artifact {
	return GetTrackerArtifactsAll[Artifact](c, id, values, query, errRcv)
}

// The GetTrackerArtifactsAll method returns all artifacts of the tracker id matching the query and following the values option, as raw JSON structure.
func (c *Client) GetTrackerArtifactsAllRaw(id int, values string, query string, errRcv ErrorReceiver) <-chan interface{} {
	return GetTrackerArtifactsAll[interface{}](c, id, values, query, errRcv)
}

// Deprecated by GetTrackerArtifacts
func (c *Client) FindArtifacts(id int, field_name string, field_value string, opt *PaginationOptions) ([]Artifact, *PaginationResponse, error) {
	query := fmt.Sprintf("%s = %s", field_name, field_value)
	v := url.Values{}
	v.Set("expert_query", query)
	url := fmt.Sprintf("/api/trackers/%d/artifacts", id)
	return RequestPaginatedJSON[Artifact](c, "GET", url, &v, opt, nil)
}

// Deprecated
func (c *Client) FindFileField(id int, field_name string) *TrackerField {
	tracker, err := c.GetTracker(id)
	if err != nil {
		return nil
	}

	for _, current := range tracker.Fields {
		if current.Type == "file" && current.Name == field_name {
			return &current
		}
	}
	return nil
}

// The GetTrackerReports method returns all reports of the tracker id.
// It is paginated
//
// See https://tuleap.net/api/explorer/#/trackers/tuleap%5CTracker%5CREST%5Cv1%5CTrackersResourceRetrieveReports
func (c *Client) GetTrackerReports(id int, opt *PaginationOptions) ([]TrackerReport, *PaginationResponse, error) {
	url := fmt.Sprintf("/api/trackers/%d/tracker_reports", id)
	return RequestPaginatedJSON[TrackerReport](c, "GET", url, nil, opt, nil)
}

// The GetTrackerReportsAll method returns all reports of the tracker id.
func (c *Client) GetTrackerReportsAll(id int, errRcv ErrorReceiver) <-chan TrackerReport {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]TrackerReport, *PaginationResponse, error) {
		return c.GetTrackerReports(id, opt)
	}, errRcv)
}

// The FindTrackerReportByLabel method returns the first report matching the provided label.
func (c *Client) FindTrackerReportByLabel(id int, label string) (*TrackerReport, error) {
	var res = TrackerReport{Id: -1}
	var err error
	catcher := CatcherErrorCallback(&err)
	for report := range c.GetTrackerReportsAll(id, catcher) {
		if res.Id == -1 && report.Label == label {
			return &report, nil
		}
	}
	return nil, fmt.Errorf("failed to find report with label '%s' in tracker %d", label, id)
}
