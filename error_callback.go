//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"os"
)

// The ErrorReceiver receive an error
type ErrorReceiver interface {
	Error(error) error
}

// A wrapper around function callback
type ErrorCallback struct {
	// The callback function
	callback func(error, interface{}) error
	// The callback data provided to orient callback function
	data interface{}
}

func (e ErrorCallback) Error(err error) error {
	return e.callback(err, e.data)
}

// A default implementation for error callback.
// It simply print error on stderr.
var DefaultErrorCallback = ErrorCallback{
	callback: func(err error, data interface{}) error {
		_, err = fmt.Fprintln(os.Stderr, err)
		return err
	},
	data: nil,
}

// CatcherErrorCallback returns an ErrorReceiver allowing to retrieve the error
func CatcherErrorCallback(ref *error) *ErrorCallback {
	return &ErrorCallback{
		callback: func(err error, i interface{}) error {
			the_err := i.(*error)
			*the_err = err
			return nil
		},
		data: ref,
	}
}
