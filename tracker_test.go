//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"encoding/json"
	"reflect"
	"testing"
)

func TestSemantics(t *testing.T) {
	s := `
	{
		"title": {
		  "field_id": 62014
		},
		"status": {
		  "field_id": 62002,
		  "value_ids": [
			56229,
			56230
		  ]
		}
	  }	`
	var semantics TrackerSemantics
	err := json.Unmarshal([]byte(s), &semantics)
	if err != nil {
		t.Errorf("Failed to decode: %s", err)
	}
	if semantics.Title.Id != 62014 {
		t.Errorf("Failed to decode Title: read %d", semantics.Title.Id)
	}
	if semantics.Status.Id != 62002 {
		t.Errorf("Failed to decode Status: read %d", semantics.Status.Id)
	}
}

func TestFields(t *testing.T) {

	tr := Tracker{Fields: []TrackerField{
		{Id: 1, Label: "One", Name: "one"},
		{Id: 2, Label: "Two", Name: "two"},
		{Id: 3, Label: "Three", Name: "three"},
	}}

	var tf *TrackerField

	tf = tr.GetTrackerFieldById(0)
	if tf != nil {
		t.Errorf("Got %v", tf)
	}

	tf = tr.GetTrackerFieldById(1)
	if tf == nil || tf.Id != 1 {
		t.Errorf("Got %v", tf)
	}

	tf = tr.GetTrackerFieldByLabel("Inex")
	if tf != nil {
		t.Errorf("Got %v", tf)
	}

	tf = tr.GetTrackerFieldByLabel("Two")
	if tf == nil || tf.Id != 2 {
		t.Errorf("Got %v", tf)
	}

	tf = tr.GetTrackerFieldByName("inex")
	if tf != nil {
		t.Errorf("Got %v", tf)
	}

	tf = tr.GetTrackerFieldByName("three")
	if tf == nil || tf.Id != 3 {
		t.Errorf("Got %v", tf)
	}

	names := tr.GetTrackerFieldNames()
	if len(names) != 3 && reflect.DeepEqual(names, []string{"one", "two", "three"}) {
		t.Errorf("Got %v", names)
	}

	labels := tr.GetTrackerFieldLabels()
	if len(labels) != 3 && reflect.DeepEqual(labels, []string{"One", "Two", "Three"}) {
		t.Errorf("Got %v", labels)
	}
}
