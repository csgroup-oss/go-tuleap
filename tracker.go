//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

type TrackerSemantics struct {
	Title  TrackerField `json:"title"`
	Status TrackerField `json:"status"`
}

type TrackerField struct {
	Id    int    `json:"field_id"`
	Label string `json:"label"`
	Name  string `json:"name"`
	Type  string `json:"type"`
}

type Tracker struct {
	Id        int              `json:"id"`
	Label     string           `json:"label"`
	Fields    []TrackerField   `json:"fields"`
	Semantics TrackerSemantics `json:"semantics"`
}

// The GetTrackerField method returns the 'first' TrackerField matching the predicate function.
func (t Tracker) GetTrackerField(predicate func(tf TrackerField) bool) *TrackerField {
	for _, tf := range t.Fields {
		if predicate(tf) {
			return &tf
		}
	}
	return nil
}

// The GetTrackerFieldById method returns the TrackerField of the given id.
func (t Tracker) GetTrackerFieldById(id int) *TrackerField {
	return t.GetTrackerField(func(tf TrackerField) bool { return tf.Id == id })
}

// The GetTrackerFieldByName method returns the TrackerField of the given name.
func (t Tracker) GetTrackerFieldByName(name string) *TrackerField {
	return t.GetTrackerField(func(tf TrackerField) bool { return tf.Name == name })
}

// The GetTrackerFieldByLabel method returns the 'first' TrackerField of the given label.
func (t Tracker) GetTrackerFieldByLabel(label string) *TrackerField {
	return t.GetTrackerField(func(tf TrackerField) bool { return tf.Label == label })
}

// The GetTrackerFieldLabels method returns all fields' labels.
func (t Tracker) GetTrackerFieldLabels() []string {
	var labels []string
	for _, tf := range t.Fields {
		labels = append(labels, tf.Label)
	}
	return labels
}

// The GetTrackerFieldNames method returns all fields' names.
func (t Tracker) GetTrackerFieldNames() []string {
	var names []string
	for _, tf := range t.Fields {
		names = append(names, tf.Name)
	}
	return names
}
