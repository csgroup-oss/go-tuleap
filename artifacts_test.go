//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"testing"
)

// Test to serialize
func TestSerializeUpdate(t *testing.T) {
	tests := []struct {
		name     string
		data     UpdateArtifactPayload
		expected string
	}{
		{
			name: "Nothing",
			data: UpdateArtifactPayload{
				Values:  nil,
				Comment: nil,
			},
			expected: `{}`,
		},
		{
			name: "Empty and no comment",
			data: UpdateArtifactPayload{
				Values:  []ArtifactValue{},
				Comment: nil,
			},
			expected: `{}`,
		},
		{
			name: "Empty comment",
			data: UpdateArtifactPayload{
				Values:  nil,
				Comment: &Comment{Body: "", Format: "text"},
			},
			expected: `{"comment":{"body":"","format":"text"}}`,
		},
		{
			name: "Some data",
			data: UpdateArtifactPayload{
				Values: []ArtifactValue{
					{
						Id:    1,
						Value: "one",
						Type:  "string",
					},
				},
				Comment: &Comment{Body: "One comment", Format: "text"},
			},
			expected: `{"values":[{"field_id":1,"type":"string","value":"one"}],"comment":{"body":"One comment","format":"text"}}`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			actual, err := dumpSingle(test.data)
			if err != nil {
				t.Errorf("Failed to dump data: %v", err)
			}
			if strings.TrimSpace(actual) != test.expected {
				t.Errorf("Expected '%s', got '%s'", test.expected, actual)
			}
		})
	}
}

// dumpSingle dumps a single value.
func dumpSingle(data interface{}) (string, error) {
	// JSON
	output := bytes.NewBuffer(nil)
	encoder := json.NewEncoder(output)
	// Encode as JSON
	if err := encoder.Encode(data); err != nil {
		return "", fmt.Errorf("Failed to encode data: %v", err)
	}
	return output.String(), nil
}
