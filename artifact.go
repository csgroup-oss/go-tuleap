//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

type ArtifactValue struct {
	Id               int               `json:"field_id"`
	Label            string            `json:"label,omitempty"`
	Type             string            `json:"type,omitempty"`
	Value            interface{}       `json:"value,omitempty"`
	Values           []interface{}     `json:"values,omitempty"`
	FileDescriptions []FileDescription `json:"file_descriptions,omitempty"`
}

// ContainsFile checks for the presence of a named file.
func (a *ArtifactValue) ContainsFile(name string) bool {
	for _, current := range a.FileDescriptions {
		if current.Name == name {
			return true
		}
	}
	return false
}

// HasFile indicates if there is at least one file.
func (a *ArtifactValue) HasFile() bool {
	return len(a.FileDescriptions) > 0
}

// HasValue check for nullity of value.
func (a *ArtifactValue) HasValue() bool {
	return a.Value != nil
}

// AppendValue appends an int value.
func (a *ArtifactValue) AppendValue(value int) {
	var v []int
	if a.Value != nil {
		v = a.Value.([]int)
	}
	a.Value = append(v, value)
}

type Artifact struct {
	Id      int             `json:"id"`
	Values  []ArtifactValue `json:"values"`
	Tracker Tracker         `json:"tracker"`
}

// GetValue a ArtifactValue matching the predicate.
func (a Artifact) GetValue(predicate func(ArtifactValue) bool) *ArtifactValue {
	var res *ArtifactValue
	for i, f := range a.Values {
		if predicate(f) {
			res = &a.Values[i]
		}
	}
	return res
}

// GetValueById retrieves the ArtifactValue by its id.
func (a Artifact) GetValueById(id int) *ArtifactValue {
	return a.GetValue(func(v ArtifactValue) bool { return v.Id == id })
}

// GetValueByLabel retrieves the ArtifactValue by its label.
func (a Artifact) GetValueByLabel(label string) *ArtifactValue {
	return a.GetValue(func(v ArtifactValue) bool { return v.Label == label })
}

type Comment struct {
	Body   string `json:"body"`
	Format string `json:"format"`
}

type Changeset struct {
	Id               int             `json:"id"`
	ArtifactId       int             `json:"artifact_id"`
	SubmittedBy      User            `json:"submitted_by_details"`
	SubmittedOn      string          `json:"submitted_on"`
	LastModifiedBy   User            `json:"last_modified_by"`
	LastModifiedDate string          `json:"last_modified_date"`
	Values           []ArtifactValue `json:"values"`
	LastComment      Comment         `json:"last_comment"`
}
