//
// Copyright 2024, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
)

// The GetFrsRelease method returns the Release from id.
//
// See https://tuleap.net/api/explorer/#/frs_release/tuleap%5CFRS%5CREST%5Cv1%5CReleaseResourceRetrieveId
func (c *Client) GetFrsRelease(id int) (*FrsRelease, error) {
	u := fmt.Sprintf("/api/frs_release/%d", id)
	var output FrsRelease
	_, err := c.RequestJSON("GET", u, nil, nil, &output)
	return &output, err
}

// CreateFrsRelease creates a new FrsRelease from a body.
//
// See https://tuleap.net/api/explorer/#/frs_release/createTuleap%5CFRS%5CREST%5Cv1%5CReleaseResource
func (c *Client) CreateFrsRelease(body interface{}) (*FrsRelease, error) {
	var output FrsRelease
	_, err := c.RequestJSON("POST", "/api/frs_release", nil, body, &output)
	return &output, err
}

// See https://tuleap.net/api/explorer/#/frs_release/tuleap%5CFRS%5CREST%5Cv1%5CReleaseResourceRetrieveFiles
func getFrsReleaseFiles(c *Client, releaseId int, opt *PaginationOptions) ([]FileDescription, *PaginationResponse, error) {
	var result []FileDescription
	path := fmt.Sprintf("/api/frs_release/%d/files", releaseId)
	// This endpoints does not returns an array but a collection struct.
	// We have to extract the array.
	var output FrsReleaseFiles
	resp, err := c.RequestPaginatedJSON("GET", path, nil, opt, nil, &output)
	if err == nil {
		result = output.Files
	}
	return result, resp, err
}

// The GetFrsReleaseFilesAll method returns all files of a given FrsRelease
func (c *Client) GetFrsReleaseFilesAll(releaseId int, errRcv ErrorReceiver) <-chan FileDescription {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]FileDescription, *PaginationResponse, error) {
		return getFrsReleaseFiles(c, releaseId, opt)
	}, errRcv)
}
