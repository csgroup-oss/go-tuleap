//
// Copyright 2024, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"io"

	"github.com/eventials/go-tus"
)

// UploadFile uploads a file.
// The path is relative to the Client Url.
func (c *Client) UploadFile(path string, name string, rc io.ReadCloser, size int64) error {
	config := tus.DefaultConfig()

	// Set the tus client authentication.
	c.SetAuth(&config.Header)

	// Create URL
	url := fmt.Sprintf("%s%s", c.Url, path)

	// create the tus client.
	client, err := tus.NewClient(url, config)
	if err != nil {
		return fmt.Errorf("Failed to create TUS client: %w", err)
	}

	// create an upload from a file.

	metadata := map[string]string{
		"filename": name,
	}

	upload := tus.NewUpload(rc, size, metadata, "")

	// create the uploader.
	uploader := tus.NewUploader(client, url, upload, 0)

	// start the uploading process.
	err = uploader.Upload()

	return err
}
