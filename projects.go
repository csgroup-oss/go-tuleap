//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"net/url"
)

// GetProject returns the project from its id.
//
// See https://tuleap.net/api/explorer/#/projects/%5CTuleap%5CProject%5CREST%5Cv1%5CProjectResourceRetrieveId
func (c *Client) GetProject(id int) (*Project, error) {
	u := fmt.Sprintf("/api/projects/%d", id)
	var output Project
	_, err := c.RequestJSON("GET", u, nil, nil, &output)
	return &output, err
}

// The GetProjects method returns all projects satisfying the passed query, all if query is empty.
// It is paginated.
//
// See https://tuleap.net/api/explorer/#/projects/retrieve%5CTuleap%5CProject%5CREST%5Cv1%5CProjectResource
func (c *Client) GetProjects(query string, opt *PaginationOptions) ([]Project, *PaginationResponse, error) {
	u := "/api/projects/"
	v := url.Values{}
	if query != "" {
		v.Set("query", query)
	}
	return RequestPaginatedJSON[Project](c, "GET", u, &v, opt, nil)

}

// The GetProjectsAll method returns all projects satisfying the passed query, all if query is empty.
func (c *Client) GetProjectsAll(query string, errRcv ErrorReceiver) <-chan Project {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]Project, *PaginationResponse, error) {
		return c.GetProjects(query, opt)
	}, errRcv)
}

// The GetProjectsByShortname method returns the list of projects having the requested shortname.
// It is paginated.
func (c *Client) GetProjectsByShortname(shortname string, opt *PaginationOptions) ([]Project, *PaginationResponse, error) {
	query := fmt.Sprintf(`{"shortname":"%s"}`, shortname)
	return c.GetProjects(query, opt)
}

// The GetProjectsByShortnameAll method returns all projects having the requested shortname.
func (c *Client) GetProjectsByShortnameAll(shortname string, errRcv ErrorReceiver) <-chan Project {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]Project, *PaginationResponse, error) {
		return c.GetProjectsByShortname(shortname, opt)
	}, errRcv)
}

// The GetProjectByLabel method returns the 'first' project having the requested label.
func (c *Client) GetProjectByLabel(label string) (*Project, error) {
	var res Project
	var the_err error
	catcher := CatcherErrorCallback(&the_err)
	for project := range c.GetProjectsAll("", catcher) {
		if res.Id == 0 && project.Label == label {
			res = project
		}
	}
	return &res, the_err
}

// The GetProjectsByLabelAll method retrieve all projects having the requested label.
func (c *Client) GetProjectsByLabelAll(label string, errRcv ErrorReceiver) <-chan Project {
	return c.GetProjectsByPredicateAll(func(proj Project) bool {
		return proj.Label == label
	}, errRcv)
}

// The GetProjectsByPredicateAll method retrieve all projects matching the passed predicate function.
func (c *Client) GetProjectsByPredicateAll(predicate func(Project) bool, errRcv ErrorReceiver) <-chan Project {
	ch := make(chan Project)

	go func() {
		for project := range c.GetProjectsAll("", errRcv) {
			if predicate(project) {
				ch <- project
			}
		}
		close(ch)
	}()
	return ch
}

// The GetProjectTrackers method retrieve the trackers of the id project satisfying the query.
// It is paginated.
//
// See https://tuleap.net/api/explorer/#/projects/tuleap%5CTracker%5CREST%5Cv1%5CProjectTrackersResourceRetrieveTrackers
func (c *Client) GetProjectTrackers(id int, query string, opt *PaginationOptions) ([]Tracker, *PaginationResponse, error) {
	u := fmt.Sprintf("/api/projects/%d/trackers", id)
	v := url.Values{}
	v.Set("representation", "minimal")
	if query != "" {
		v.Set("query", query)
	}
	return RequestPaginatedJSON[Tracker](c, "GET", u, &v, opt, nil)
}

// The GetProjectTrackers method retrieve all the trackers of the id project satisfying the query.
func (c *Client) GetProjectTrackersAll(id int, query string, errRcv ErrorReceiver) <-chan Tracker {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]Tracker, *PaginationResponse, error) {
		return c.GetProjectTrackers(id, query, opt)
	}, errRcv)
}

// The GetProjectTrackerByLabel method retrieve the 'first' tracker of the pid project having the requested label.
func (c *Client) GetProjectTrackerByLabel(id int, label string) (*Tracker, error) {
	var res Tracker
	var err error
	catcher := CatcherErrorCallback(&err)
	for tracker := range c.GetProjectTrackersAll(id, "", catcher) {
		if res.Id == 0 && tracker.Label == label {
			res = tracker
		}
	}
	return &res, err
}

// GetProjectGroups returns the list of groups of the project.
//
// See https://tuleap.net/api/explorer/#/projects/%5CTuleap%5CProject%5CREST%5Cv1%5CProjectResourceRetrieveUserGroups
func (c *Client) GetProjectGroups(id int) ([]UserGroup, error) {
	path := fmt.Sprintf("/api/projects/%d/user_groups", id)
	var output []UserGroup
	_, err := c.RequestJSON("GET", path, nil, nil, &output)
	return output, err
}

// The GetProjectFrsPackages method retrieve the FRS packages of the id project.
// It is paginated.
//
// See https://tuleap.net/api/explorer/#/projects/tuleap%5CFRS%5CREST%5Cv1%5CProjectResourceRetrieveFRSPackages
func (c *Client) GetProjectFrsPackages(id int, opt *PaginationOptions) ([]FrsPackage, *PaginationResponse, error) {
	u := fmt.Sprintf("/api/projects/%d/frs_packages", id)
	return RequestPaginatedJSON[FrsPackage](c, "GET", u, nil, opt, nil)
}

// The GetProjectFrsPackagesAll method retrieve all the FRS package of the id project.
func (c *Client) GetProjectFrsPackagesAll(id int, errRcv ErrorReceiver) <-chan FrsPackage {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]FrsPackage, *PaginationResponse, error) {
		return c.GetProjectFrsPackages(id, opt)
	}, errRcv)
}

// GetProjectGitRepositories method retrieves the Git repositories of a project.
// It is paginated.
//
// See https://tuleap.net/api/explorer/#/projects/tuleap%5CGit%5CREST%5Cv1%5CGitProjectResourceRetrieveGit
func (c *Client) GetProjectGitRepositories(id int, opt *PaginationOptions) ([]GitRepository, *PaginationResponse, error) {
	u := fmt.Sprintf("/api/projects/%d/git", id)
	var output struct {
		Collection []GitRepository `json:"repositories"`
	}
	resp, err := c.RequestPaginatedJSON("GET", u, nil, opt, nil, &output)
	return output.Collection, resp, err
}

// The GetProjectGitRepositoriesAll method retrieves all the Git repositories of the project.
func (c *Client) GetProjectGitRepositoriesAll(id int, errRcv ErrorReceiver) <-chan GitRepository {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]GitRepository, *PaginationResponse, error) {
		return c.GetProjectGitRepositories(id, opt)
	}, errRcv)
}

// GetProjectGitJenkinsServers method retrieves the Git Jenkins servers of a project.
// It is paginated.
// See https://tuleap.net/api/explorer/#/projects/tuleap%5CHudsonGit%5CREST%5Cv1%5CGitJenkinsServersResourceRetrieveGitJenkinsServers
func (c *Client) GetProjectGitJenkinsServers(id int, opt *PaginationOptions) ([]GitJenkinsServer, *PaginationResponse, error) {
	u := fmt.Sprintf("/api/projects/%d/git_jenkins_servers", id)
	var output struct {
		Collection []GitJenkinsServer `json:"git_jenkins_servers_representations"`
	}
	resp, err := c.RequestPaginatedJSON("GET", u, nil, opt, nil, &output)
	return output.Collection, resp, err
}

// The GetProjectGitJenkinsServersAll method retrieves all the Git Jenkins servers of the project.
func (c *Client) GetProjectGitJenkinsServersAll(id int, errRcv ErrorReceiver) <-chan GitJenkinsServer {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]GitJenkinsServer, *PaginationResponse, error) {
		return c.GetProjectGitJenkinsServers(id, opt)
	}, errRcv)
}

// GetProjectSvnRepositories method retrieves the SVN repositories of a project.
// It is paginated.
//
// See https://gaia.si.c-s.fr/api/explorer/#/projects/%5CTuleap%5CProject%5CREST%5Cv1%5CProjectResourceRetrieveSvn
func (c *Client) GetProjectSvnRepositories(id int, opt *PaginationOptions) ([]SvnRepository, *PaginationResponse, error) {
	u := fmt.Sprintf("/api/projects/%d/svn", id)
	var output struct {
		Collection []SvnRepository `json:"repositories"`
	}
	resp, err := c.RequestPaginatedJSON("GET", u, nil, opt, nil, &output)
	return output.Collection, resp, err
}

// The GetProjectSvnRepositoriesAll method retrieves all the SVN repositories of the project.
func (c *Client) GetProjectSvnRepositoriesAll(id int, errRcv ErrorReceiver) <-chan SvnRepository {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]SvnRepository, *PaginationResponse, error) {
		return c.GetProjectSvnRepositories(id, opt)
	}, errRcv)
}
