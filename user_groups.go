package tuleap

import "fmt"

// GetGroupUsersAll method returns all users of a given user group.
//
// See https://tuleap.net/api/explorer/#/user_groups/tuleap%5CProject%5CREST%5Cv1%5CUserGroupResourceRetrieveUsers
func (c *Client) GetGroupUsersAll(id string, errRcv ErrorReceiver) <-chan User {
	path := fmt.Sprintf("/api/user_groups/%s/users", id)
	return RequestPaginatedJSONAll[User](c, "GET", path, nil, nil, errRcv)
}
