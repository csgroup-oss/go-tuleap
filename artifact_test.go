//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import "testing"

// Test HasValue on ArtifactValue
func TestArtifactValueHasValue(t *testing.T) {
	empty := ArtifactValue{}
	if empty.HasValue() {
		t.Errorf("Empty ArtifactValue should not have a value")
	}

	notEmpty := ArtifactValue{Value: "foo"}
	if !notEmpty.HasValue() {
		t.Errorf("ArtifactValue with value should have a value")
	}
}

func TestArtifactValueHasFile(t *testing.T) {
	empty := ArtifactValue{}
	if empty.HasFile() {
		t.Errorf("Empty ArtifactValue should not have a file")
	}
	foo := FileDescription{
		Name: "foo",
		Id:   1,
	}
	notEmpty := ArtifactValue{FileDescriptions: []FileDescription{foo}}
	if !notEmpty.HasFile() {
		t.Errorf("ArtifactValue with file should have a file")
	}
}

func TestArtifactGetValue(t *testing.T) {
	art := Artifact{
		Values: []ArtifactValue{
			{Id: 1, Label: "One"},
			{Id: 2, Label: "Two"},
			{Id: 3, Label: "Three"},
		},
	}
	if art.GetValueById(0) != nil {
		t.Errorf("Artifact should not have a value 0")
	}
	if art.GetValueById(1) == nil {
		t.Errorf("Artifact should have a value 1")
	}
	if art.GetValueByLabel("Two") == nil {
		t.Errorf("Artifact should have a value Two")
	}
	if art.GetValueByLabel("inex") != nil {
		t.Errorf("Artifact should not have a value inex")
	}
}
