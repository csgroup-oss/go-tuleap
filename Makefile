all: test

test:
	go test -v ./...

clean:
	go clean

version:
	go version

swagger.json:
	curl -q https://tuleap.net/api/explorer/swagger.json -O $@

endpoints.lst: swagger.json
	jq -r '.paths | keys | .[]' < swagger.json > $@

covered.lst:
	git grep -h -o '"/api/.*"' -- "*.go" | sed -e 's/"//g' -e 's/?.*//' -e 's;/$$;;' | sort -u > $@

api-coverage: endpoints.lst covered.lst
	sed -n -e '1,/begin-covered/p' -e '/end-covered/,$$p' < coverage.md > coverage.tpl
	sed -e "s/%d/{id}/" -e "s/^/* /" covered.lst > covered.md
	sed '/begin-covered/r covered.md' < coverage.tpl > coverage.md

clean-coverage:
	rm -f endpoints.lst covered.lst
