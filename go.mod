module gitlab.com/csgroup-oss/go-tuleap

go 1.18

require (
	github.com/eventials/go-tus v0.0.0-20220610120217-05d0564bb571
	github.com/stretchr/testify v1.5.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
