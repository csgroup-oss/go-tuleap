//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

type ClientAuth struct {
	Token    string
	Username string
	Password string
}

type Client struct {
	Url  string
	Auth ClientAuth

	client *http.Client
}

type RequestOptions interface {
	GetQuery() url.Values
}

type PaginationOptions struct {
	// For paginated result sets, page of results to retrieve.
	Offset int

	// For paginated result sets, the number of results to include per page.
	Limit int
}

func (p PaginationOptions) GetQuery() url.Values {
	values := url.Values{}
	if p.Offset > 0 {
		values.Add("offset", fmt.Sprint(p.Offset))
	}
	if p.Limit > 0 {
		values.Add("limit", fmt.Sprint(p.Limit))
	}
	return values
}

type PaginationResponse struct {
	Limit    int
	LimitMax int
	Offset   int
	Size     int
}

// NewClient creates a new Tuleap client.
// server is hostname
func NewClient(server string, auth ClientAuth, tlsOpts ...*tls.Config) (*Client, error) {

	customTransport := http.DefaultTransport.(*http.Transport).Clone()
	for _, tls := range tlsOpts {
		customTransport.TLSClientConfig = tls
	}
	httpClient := &http.Client{Transport: customTransport}

	url := "https://" + server

	return &Client{
		Url:  url,
		Auth: auth,

		client: httpClient,
	}, nil
}

// basicAuth generates a basic auth string from username and password.
func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

// SetAuth sets the Authorization header for the given Request.
func (c *Client) SetAuth(header *http.Header) {
	if c.Auth.Token != "" {
		header.Set("X-Auth-AccessKey", c.Auth.Token)
	} else if c.Auth.Username != "" {
		header.Set("Authorization", "Basic "+basicAuth(c.Auth.Username, c.Auth.Password))
	}
}

// SetTransport sets the Transport for the Client.
// Could be useful for debugging.
func (c *Client) SetTransport(transport http.RoundTripper) {
	c.client.Transport = transport
}

// The Do method executes a given Request using the Client.
func (c *Client) Do(req *http.Request) (*http.Response, error) {
	c.SetAuth(&req.Header)
	return c.client.Do(req)
}

// The Request method creates and executes a Request using the Client.
func (c *Client) Request(method string, path string, query *url.Values, header http.Header, body *bytes.Buffer) (*http.Response, error) {
	url := c.Url + path
	if query != nil && len(*query) > 0 {
		url = url + "?" + query.Encode()
	}

	var b io.Reader
	b = nil
	if body != nil {
		b = body
	}
	req, err := http.NewRequest(method, url, b)
	if err != nil {
		return nil, err
	}
	// Add specific headers
	for k, v := range header {
		req.Header[k] = v
	}

	return c.Do(req)
}

// The RequestJSON method creates and executes a Request receiving and returning JSON payload.
func (c *Client) RequestJSON(method string, path string, query *url.Values, input interface{}, output interface{}) (*http.Response, error) {

	header := make(http.Header)
	header.Set("Accept", "application/json")

	// Input Body
	var body *bytes.Buffer
	body = nil
	if input != nil {
		// Encode JSON
		b, err := json.Marshal(input)
		if err != nil {
			return nil, err
		}
		body = bytes.NewBuffer(b)
	}

	// Do Request
	resp, err := c.Request(method, path, query, header, body)
	if err != nil {
		return resp, err
	}

	// Output Body
	defer resp.Body.Close()
	dec := json.NewDecoder(resp.Body)

	// Check response
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		var clientError ClientErrorPayload
		if dec.More() {
			if err := dec.Decode(&clientError); err != nil {
				return resp, fmt.Errorf("Failed to decode error payload for HTTP Status %d: %v", resp.StatusCode, err)
			}
			return resp, clientError.Error
		} else {
			return resp, errors.New(resp.Status)
		}
	}

	if output != nil && dec.More() {
		if err = dec.Decode(&output); err != nil {
			return resp, fmt.Errorf("Failed to decode payload: %v", err)
		}
	}
	return resp, nil
}

// The RequestPaginatedJSON method creates and executes a Request receiving and returning JSON payload
// dealing with the Pagination protocol.
func (c *Client) RequestPaginatedJSON(method string, path string, query *url.Values, opt *PaginationOptions, input interface{}, output interface{}) (*PaginationResponse, error) {
	// Append query
	if opt != nil {
		for key, values := range opt.GetQuery() {
			if query == nil {
				query = &url.Values{}
			}
			for _, value := range values {
				query.Add(key, value)
			}
		}
	}

	resp, err := c.RequestJSON(method, path, query, input, output)
	if err != nil {
		return nil, fmt.Errorf("Failed to execute request %s on path %s: %v", method, path, err)
	}

	presp := PaginationResponse{
		Limit:    getHeaderAsInt(resp.Header, "x-pagination-limit", 0),
		Offset:   getHeaderAsInt(resp.Header, "x-pagination-offset", 0),
		LimitMax: getHeaderAsInt(resp.Header, "x-pagination-limit-max", 0),
		Size:     getHeaderAsInt(resp.Header, "x-pagination-size", 0),
	}

	return &presp, nil
}

func getHeaderAsInt(header http.Header, key string, def int) int {
	res, err := strconv.Atoi(header.Get(key))
	if err != nil {
		// Failed to retrieve header
		return def
	} else {
		return res
	}
}

// RequestJSON is an helper function to request a single element and return the expected type
func RequestJSON[T interface{}](c *Client, method string, path string, query *url.Values, input interface{}) (T, error) {
	var output T
	_, err := c.RequestJSON(method, path, query, input, &output)
	return output, err
}

// RequestPaginatedJSON is an helper function to request a paginated endpoint and return an array of the expected type
func RequestPaginatedJSON[T interface{}](c *Client, method string, path string, query *url.Values, opt *PaginationOptions, input interface{}) ([]T, *PaginationResponse, error) {
	var output []T
	resp, err := c.RequestPaginatedJSON(method, path, query, opt, input, &output)
	return output, resp, err
}

// RequestPaginatedJSONAll is an helper function to iterate over a paginated endpoint and return a channel of the expected type
func RequestPaginatedJSONAll[T interface{}](c *Client, method string, path string, query *url.Values, input interface{}, errRcv ErrorReceiver) <-chan T {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]T, *PaginationResponse, error) {
		return RequestPaginatedJSON[T](c, method, path, query, opt, input)
	}, errRcv)
}

// DoPaginatedAll is an helper function to iterate over a paginated endpoint, handled by a provided function
func DoPaginatedAll[T interface{}](fct func(*PaginationOptions) ([]T, *PaginationResponse, error), errRcv ErrorReceiver) <-chan T {
	ch := make(chan T)

	go func() {
		opt := PaginationOptions{}
		for {
			items, resp, err := fct(&opt)
			if err != nil {
				if errRcv != nil {
					err = errRcv.Error(err)
					if err != nil {
						fmt.Fprintln(os.Stderr, err)
					}
				}
				break
			}

			for _, item := range items {
				ch <- item
			}

			// Update the page number to get the next page.
			opt.Limit = resp.Limit
			opt.Offset += opt.Limit
			// Exit the loop when we've seen all pages.
			if opt.Offset > resp.Size {
				break
			}
		}
		close(ch)
	}()
	return ch
}
