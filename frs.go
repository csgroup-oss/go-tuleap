//
// Copyright 2024, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

type FrsPackage struct {
	Id      int     `json:"id"`
	Label   string  `json:"label"`
	Project Project `json:"project"`
}

type FrsRelease struct {
	Name        string     `json:"name"`
	Id          int        `json:"id"`
	Package     FrsPackage `json:"package"`
	Project     Project    `json:"project"`
	ReleaseNote string     `json:"release_note"`
	Status      string     `json:"status"`
	ReleaseDate string     `json:"release_date"`
}

type FrsFile struct {
	Id           int    `json:"id"`
	Name         string `json:"name"`
	Date         string `json:"date"`
	Size         int    `json:"file_size"`
	DownloadUrl  string `json:"download_url"`
	NbDownloads  int    `json:"nb_download"`
	ComputedMD5  string `json:"computed_md5"`
	ReferenceMD5 string `json:"reference_md5"`
	Type         string `json:"type"`
}

// Non standard response for /api/frs_release/{id}/files
type FrsReleaseFiles struct {
	Files []FileDescription `json:"files"`
}

// Non standard response for /api/frs_package/{id}/frs_release
type FrsPackageReleases struct {
	Releases []FrsRelease `json:"collection"`
}
