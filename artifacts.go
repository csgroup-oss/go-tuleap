//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"net/url"
)

// The GetArtifact method returns the artifact id.
// See https://tuleap.net/api/explorer/#/artifacts/tuleap%5CTracker%5CREST%5Cv1%5CArtifactsResourceRetrieveArtifacts
func (c *Client) GetArtifact(id int) (*Artifact, error) {
	url := fmt.Sprintf("/api/artifacts/%d/?tracker_structure_format=minimal", id)
	var artifact Artifact
	_, err := c.RequestJSON("GET", url, nil, nil, &artifact)
	if err != nil {
		return nil, err
	}

	return &artifact, nil
}

type UpdateArtifactPayload struct {
	Values  []ArtifactValue `json:"values,omitempty"`
	Comment *Comment        `json:"comment,omitempty"`
}

// The UpdateArtifactValue method updates the field of the artifact.
// See https://tuleap.net/api/explorer/#/artifacts/tuleap%5CTracker%5CREST%5Cv1%5CArtifactsResourceUpdateId
func (c *Client) UpdateArtifact(id int, values []ArtifactValue, comment *Comment) error {
	if Dryrun {
		// Do nothing
		return nil
	}
	body := UpdateArtifactPayload{
		Values:  values,
		Comment: comment,
	}

	url := fmt.Sprintf("/api/artifacts/%d", id)
	_, err := c.RequestJSON("PUT", url, nil, body, nil)
	return err
}

// The UpdateArtifactValue method updates the field of the artifact.
//
// Deprecated: use UpdateArtifact instead
func (c *Client) UpdateArtifactValue(id int, field *ArtifactValue) error {
	if Dryrun {
		// Do nothing
		return nil
	}
	return c.UpdateArtifact(id, []ArtifactValue{*field}, nil)
}

// The UpdateArtifactComment method adds a comment to an artifact.
//
// Deprecated: use UpdateArtifact instead
func (c *Client) UpdateArtifactComment(id int, comment string) error {
	if Dryrun {
		// Do nothing
		return nil
	}
	return c.UpdateArtifact(id, nil, &Comment{Body: comment, Format: "text"})
}

// The GetArtifactChangesets retrieves the changesets of an artifact.
//
// See https://tuleap.net/api/explorer/#/artifacts/tuleap%5CTracker%5CREST%5Cv1%5CArtifactsResourceRetrieveArtifactChangesets
func (c *Client) GetArtifactChangesets(id int, opt *PaginationOptions, changesets interface{}) (*PaginationResponse, error) {
	params := url.Values{}
	params.Add("fields", "all")
	url := fmt.Sprintf("/api/artifacts/%d/changesets", id)
	return c.RequestPaginatedJSON("GET", url, &params, opt, nil, changesets)
}

// The GetArtifactComments returns the comments of an artifact.
//
// See https://tuleap.net/api/explorer/#/artifacts/tuleap%5CTracker%5CREST%5Cv1%5CArtifactsResourceRetrieveArtifactChangesets
func (c *Client) GetArtifactComments(id int) ([]Comment, error) {
	params := url.Values{}
	params.Add("fields", "comments")
	url := fmt.Sprintf("/api/artifacts/%d/changesets", id)
	var changesets []Changeset
	_, err := c.RequestJSON("GET", url, &params, nil, &changesets)
	if err != nil {
		return nil, err
	}
	// Convert as collection of Comments
	var comments []Comment
	for _, changeset := range changesets {
		comments = append(comments, changeset.LastComment)
	}
	return comments, nil
}

// The AppendFileToArtifact method appends a file to an artifact.
func (c *Client) AppendFileToArtifact(artifact_id int, field *ArtifactValue, file_id int) error {
	if field.HasFile() {
		for _, current := range field.FileDescriptions {
			field.AppendValue(current.Id)
		}
	}
	field.AppendValue(file_id)
	return c.UpdateArtifactValue(artifact_id, field)
}

// See https://tuleap.net/api/explorer/#/artifacts/tuleap%5CTracker%5CREST%5Cv1%5CArtifactsResourceRetrieveArtifactChangesets
func getArtifactChangesets[T interface{}](c *Client, artifact_id int, opt *PaginationOptions) ([]T, *PaginationResponse, error) {
	params := url.Values{}
	params.Add("fields", "all")
	url := fmt.Sprintf("/api/artifacts/%d/changesets", artifact_id)
	return RequestPaginatedJSON[T](c, "GET", url, &params, opt, nil)
}

// GetArtifactChangesetsAll returns all changesets of an artifact.
func GetArtifactChangesetsAll[T interface{}](c *Client, id int, errRcv ErrorReceiver) <-chan T {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]T, *PaginationResponse, error) {
		return getArtifactChangesets[T](c, id, opt)
	}, errRcv)
}

// The GetArtifactChangesetsRawAll method returns all changesets of an artifact, as raw JSON structure.
func (c *Client) GetArtifactChangesetsRawAll(artifact_id int, errRcv ErrorReceiver) <-chan interface{} {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]interface{}, *PaginationResponse, error) {
		return getArtifactChangesets[interface{}](c, artifact_id, opt)
	}, errRcv)
}
