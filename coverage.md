# API coverage

The following API endpoints are covered by this library.

<!--begin-covered-->
* /api/artifacts/{id}
* /api/artifacts/{id}/changesets
* /api/frs_files
* /api/frs_files/{id}
* /api/frs_packages
* /api/frs_packages/{id}
* /api/frs_packages/{id}/frs_release
* /api/frs_release
* /api/frs_release/{id}
* /api/frs_release/{id}/files
* /api/projects
* /api/projects/{id}
* /api/projects/{id}/frs_packages
* /api/projects/{id}/git
* /api/projects/{id}/git_jenkins_servers
* /api/projects/{id}/svn
* /api/projects/{id}/trackers
* /api/projects/{id}/user_groups
* /api/tracker_fields/{id}/files
* /api/tracker_reports/{id}
* /api/tracker_reports/{id}/artifacts
* /api/trackers/{id}
* /api/trackers/{id}/artifacts
* /api/trackers/{id}/tracker_reports
* /api/user_groups/%s/users
* /api/users
* /api/users/{id}
* /api/users/{id}/membership
<!--end-covered-->
