//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnmarshall(t *testing.T) {

	s := `
	{ 
		"id": "106_3", 
		"uri": "user_groups/106_3", 
		"label": "Project members", 
		"users_uri": "user_groups/106_3/users", 
		"short_name": "project_members", 
		"key": "ugroup_project_members_name_key", 
		"project": { 
		  "id": 114, 
		   "uri": "projects/114", 
		  "label": "test", 
		   "shortname": "test", 
		   "status": "active", 
		   "access": "public", 
		   "is_template": false 
		   }
		 }	`
	var ug UserGroup
	err := json.Unmarshal([]byte(s), &ug)
	if err != nil {
		t.Errorf("Failed to decode: %s", err)
	}
	assert.Equal(t, ug.Id, "106_3", "failed to decode user group.")
	assert.Equal(t, ug.Project.Id, 114, "Failed to decode project.")

}
