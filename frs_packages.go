//
// Copyright 2024, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import "fmt"

// GetFrsPackage method returns the package of the given id
//
// See https://tuleap.net/api/explorer/#/frs_packages/tuleap%5CFRS%5CREST%5Cv1%5CPackageResourceRetrieveId
func (c *Client) GetFrsPackage(id int) (*FrsPackage, error) {
	u := fmt.Sprintf("/api/frs_packages/%d", id)
	var output FrsPackage
	_, err := c.RequestJSON("GET", u, nil, nil, &output)
	return &output, err
}

// CreateFrsPackage method creates a new package from a body.
//
// See https://tuleap.net/api/explorer/#/frs_packages/createTuleap%5CFRS%5CREST%5Cv1%5CPackageResource
func (c *Client) CreateFrsPackage(body interface{}) (*FrsPackage, error) {
	var output FrsPackage
	_, err := c.RequestJSON("POST", "/api/frs_packages", nil, body, &output)
	return &output, err
}

// See https://tuleap.net/api/explorer/#/frs_packages/tuleap%5CFRS%5CREST%5Cv1%5CPackageResourceRetrieveReleases
func getFrsPackageReleases(c *Client, id int, opt *PaginationOptions) ([]FrsRelease, *PaginationResponse, error) {
	var result []FrsRelease
	path := fmt.Sprintf("/api/frs_packages/%d/frs_release", id)
	// This endpoints does not returns an array but a collection struct.
	// We have to extract the array.
	var output FrsPackageReleases
	resp, err := c.RequestPaginatedJSON("GET", path, nil, opt, nil, &output)
	if err == nil {
		result = output.Releases
	}
	return result, resp, err
}

// GetFrsPackageReleasesAll method returns the releases of the given package id
func (c *Client) GetFrsPackageReleasesAll(id int, errRcv ErrorReceiver) <-chan FrsRelease {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]FrsRelease, *PaginationResponse, error) {
		return getFrsPackageReleases(c, id, opt)
	}, errRcv)
}
