//
// Copyright 2024, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
)

// CreateTrackerFieldFile creates a new file for a tracker field.
//
// See https://tuleap.net/api/explorer/#/tracker_fields/tuleap%5CTracker%5CREST%5Cv1%5CTrackerFieldsResourceCreateFiles
func (c *Client) CreateTrackerFieldFile(field_id int, file_name string, file_size int64, file_type string) (*File, error) {
	url := fmt.Sprintf("/api/tracker_fields/%d/files", field_id)

	body := map[string]interface{}{
		"name":      file_name,
		"file_size": file_size,
		"file_type": file_type,
	}

	var new_file File
	_, err := c.RequestJSON("POST", url, nil, body, &new_file)
	if err != nil {
		return nil, err
	}
	return &new_file, nil
}
