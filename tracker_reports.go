//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"net/url"
)

// The GetTrackerReport method returns the TrackerReport from its id.
//
// See https://tuleap.net/api/explorer/#/tracker_reports/tuleap%5CTracker%5CREST%5Cv1%5CReportsResourceRetrieveId
func (c *Client) GetTrackerReport(id int) (*TrackerReport, error) {
	url := fmt.Sprintf("/api/tracker_reports/%d", id)

	var t TrackerReport
	_, err := c.RequestJSON("GET", url, nil, nil, &t)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

// See https://tuleap.net/api/explorer/#/tracker_reports/tuleap%5CTracker%5CREST%5Cv1%5CReportsResourceRetrieveArtifacts
func getTrackerReportArtifacts[T interface{}](c *Client, id int, values string, opt *PaginationOptions) ([]T, *PaginationResponse, error) {
	v := url.Values{}
	v.Set("values", values)
	url := fmt.Sprintf("/api/tracker_reports/%d/artifacts", id)
	return RequestPaginatedJSON[T](c, "GET", url, &v, opt, nil)
}

// The GetTrackerReportArtifacts method returns the artifacts of the rid tracker report, following the values option.
// It is paginated.
func (c *Client) GetTrackerReportArtifacts(id int, values string, opt *PaginationOptions) ([]Artifact, *PaginationResponse, error) {
	return getTrackerReportArtifacts[Artifact](c, id, values, opt)
}

// The GetTrackerReportArtifactsRaw method returns the artifacts as raw JSON structure of the tracker report id, following the values option.
// It is paginated.
func (c *Client) GetTrackerReportArtifactsRaw(id int, values string, opt *PaginationOptions) ([]interface{}, *PaginationResponse, error) {
	return getTrackerReportArtifacts[interface{}](c, id, values, opt)
}

// GetTrackerReportArtifactsAll returns all artifacts of the tracker report id, following the value option, filling the type T.
func GetTrackerReportArtifactsAll[T interface{}](c *Client, id int, values string, errRcv ErrorReceiver) <-chan T {
	return DoPaginatedAll(func(opt *PaginationOptions) ([]T, *PaginationResponse, error) {
		return getTrackerReportArtifacts[T](c, id, values, opt)
	}, errRcv)
}

// The GetTrackerReportArtifactsAll returns all the artifacts of the tracker report id, following the values option.
func (c *Client) GetTrackerReportArtifactsAll(id int, values string, errRcv ErrorReceiver) <-chan Artifact {
	return GetTrackerReportArtifactsAll[Artifact](c, id, values, errRcv)
}

// The GetTrackerReportArtifactsRawAll returns all the artifacts of the tracker report id, following the values option, as raw JSON structure.
func (c *Client) GetTrackerReportArtifactsRawAll(id int, values string, errRcv ErrorReceiver) <-chan interface{} {
	return GetTrackerReportArtifactsAll[interface{}](c, id, values, errRcv)
}
