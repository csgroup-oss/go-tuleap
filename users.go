//
// Copyright 2023, CS GROUP - France
// Author: Guilhem Bonnefille
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package tuleap

import (
	"fmt"
	"net/http"
	"net/url"
)

// The GetUserById method returns the user of the id.
func (c *Client) GetUserById(id int) (User, error) {
	path := fmt.Sprintf("/api/users/%d", id)

	return RequestJSON[User](c, "GET", path, nil, nil)
}

// The GetUserByUsername returns the user matching the requested username.
func (c *Client) GetUserByUsername(username string) (User, error) {
	params := url.Values{}
	params.Add("query", fmt.Sprintf("{\"username\":\"%s\"}", username))
	users, _, err := RequestPaginatedJSON[User](c, "GET", "/api/users/", &params, nil, nil)
	if len(users) > 0 {
		return users[0], err
	} else {
		return User{}, err
	}
}

// The FindUsersAll method returns all the users matching the query.
// It is paginated.
func (c *Client) FindUsers(query string, opt *PaginationOptions) ([]User, *PaginationResponse, error) {
	params := url.Values{}
	params.Add("query", query)
	return RequestPaginatedJSON[User](c, "GET", "/api/users/", &params, opt, nil)
}

// The FindUsersAll method returns all the users matching the query.
func (c *Client) FindUsersAll(query string, errRcv ErrorReceiver) <-chan User {
	params := url.Values{}
	params.Add("query", query)
	return RequestPaginatedJSONAll[User](c, "GET", "/api/users/", &params, nil, errRcv)
}

// The getUserMembership method retrieve user membership.
// scope can be "project"
// format can be "full"
func (c *Client) getUserMembership(id int, scope string, format string, output interface{}) (*http.Response, error) {
	params := url.Values{}
	if scope != "" {
		params.Add("scope", scope)
	}
	if format != "" {
		params.Add("format", format)
	}
	path := fmt.Sprintf("/api/users/%d/membership", id)

	return c.RequestJSON("GET", path, &params, nil, output)
}

// The GetUserProjectMembership method returns the list of UserGroup of the user id.
func (c *Client) GetUserProjectMembership(id int) ([]UserGroup, error) {
	var output []UserGroup
	_, err := c.getUserMembership(id, "project", "full", &output)
	if err != nil {
		return nil, err
	}
	return output, nil
}
